Rails.application.routes.draw do
  scope ":locale", locale: /#{I18n.available_locales.join("|")}/ do
    resources :registration_steps
    resources :types
    resources :titles
    resources :users, path: 'u'
    root 'pages#root'
    get '/home', to: 'pages#home'

    resources :people do
      resources :comments, only: [:index, :new, :create ], module: :people  do
        member do
          put "like", to: "comments#upvote"
        end
      end
      get '/films', to: "people#film"
    end

    resources :movies
    resources :series
    resources :settings

    get '/catalogo', to: 'catalogue#index'
    get '/dashboard', to: 'dashboard#index'

    namespace :dashboard do
      resources :titles
      resources :people
      get '/actions', to: 'actions#index'
      post '/actions/tmbd_discover', to: 'actions#tmdb_discover'
    end
  end
  devise_for :users, controllers: {omniauth_callbacks: "omniauth_callbacks", registrations: "users/registrations"}
  get '', to: redirect("/#{I18n.locale}")
end
