module ApplicationHelper
  def get_email_oauth
    if session[:omniauth_data]
      session[:omniauth_data][:email]
    else
      ""
    end
  end
  def show_year(stringDate)
    if stringDate == ""
      return ""
    end
    data = Date.parse(stringDate)
    data.strftime("%Y")
  end
  def show_age(stringDate)
    data = show_year(stringDate)
    date_now = Time.now.strftime("%Y")
    date_now.to_i - data.to_i
  end

  def current_class?(test_path)
    return 'active-menu' if current_page?(test_path)
    ''
  end
end
