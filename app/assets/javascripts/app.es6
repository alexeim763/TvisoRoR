$(document).ready(function () {
    //Init Javascript
    //NProgress.start();
    //Function slideNotification

    $('.Notification').on('click', function (ev) {
        ev.preventDefault();
        $('.NotificationSlide').toggle();
    });

    $('.SlidePeoples-content').mCustomScrollbar({
        theme: "rounded-dark",
        axis: 'x',
        autoExpandScrollbar:true,
        setHeight:"100%",
        setWidth:"100%",
        scrollButtons: {
            enable: true
        },
        advanced:{autoExpandHorizontalScroll:true}
    });

    $('#SidebarLeft').sticky({ topSpacing: 45 });

    $("#show_videoTrailer").fancybox({
        "type": "iframe",
    })

});
