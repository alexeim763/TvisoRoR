// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require jquery.mCustomScrollbar.concat.min
//= require jquery.sticky
//= require fancybox
//= require bootstrap-sprockets
//= require raphael-2.1.0.min
//= require morris
//= require custom-scripts
//= require easypiechart-data
//= require easypiechart
//= require jquery.metisMenu
//= require nprogress
//= require nprogress-turbolinks
