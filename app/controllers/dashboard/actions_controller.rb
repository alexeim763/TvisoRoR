class Dashboard::ActionsController < ApplicationController
  layout 'dashboard'
  def index
    @total_comments = Comment.count
    @total_users = User.count
    @total_titles = Title.count
    @total_people = Person.count
  end
  def tmdb_discover
    @discover = tmbdApi_params
    if @discover[:type] == "movie"
      
      how_much = @discover[:how_much].to_i
      number_repeat = (how_much/20).round
      (1..number_repeat).each do |page|
        @discover[:page] = page.to_s
        movies = Tmdb::Discover.movie(@discover)
        movies.results.each do |movie|
          new_title = Title.new
          new_title = create_object(new_title,movie) 
          new_title = Title.find_or_update_by_scrapper(new_title,new_title.tmbd_id,@discover[:language].to_sym )
        end
      end
      flash[:notice] = "Completado"
      redirect_to :back
    elsif @discover[:type] =="series"

    else
      redirect_to :back
    end

  end
  private
  def tmbdApi_params
    params.require(:scraper).permit(:sort_by, :include_adult, :type, :language, :release_from_year, :release_to_year, :how_much)
  end
  def create_object(object,result)
    #if state is update if not save
    object.title = result.title
    object.overview = result.overview
    object.tagline =  ""
    if result.poster_path == nil
      object.poster = ""
    else
      object.poster = "http://image.tmdb.org/t/p/w342/" + result.poster_path
    end
    object.type_id = 1
    if result.imdb_id == nil
      object.imbd_id = ""
    else
      object.imbd_id = result.imdb_id
    end
    object.tmbd_id = result.id
    object.vote_average = result.vote_average
    object.release_date = result.release_date
    if result.backdrop_path == nil
      object.background = ""
      object.backdrop = ""
    else
      object.background = "http://image.tmdb.org/t/p/w1280/" + result.backdrop_path 
      object.backdrop = "http://image.tmdb.org/t/p/w780/" + result.backdrop_path 
    end
    return object
  end
end
