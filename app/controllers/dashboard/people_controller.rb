class Dashboard::PeopleController < ApplicationController
before_action :find_post, only: [:show, :edit, :destroy, :update]
layout 'dashboard'
  def index
    @people = Person.paginate(:page => params[:page], :per_page => 2)
  end

  def show
    @movie = Tmdb::Discover.movie()
    raise
  end
  def other
    var = 2  
    
    while var < 4
      unless Tmdb::Movie.detail(var, language: 'es')
        @movie = Tmdb::Movie.detail(var, language: 'es')
        @cast = Tmdb::Movie.cast(var)
        @video = Tmdb::Movie.videos(var)
        @credits = Tmdb::Movie.crew(var)

        @m = Title.new
        @m.title = @movie.title
        @m.overview = @movie.overview
        @m.poster = "http://image.tmdb.org/t/p/w342/" + @movie.poster_path
        @m.tagline =  @movie.tagline
        @m.type_id = 1
        unless @video
            @m.trailer =  "http://www.youtube.com/embed/" + @video[0].key 
        end  
        

        @m.tmbd_id = @movie.id
        @m.imbd_id = @movie.imdb_id
        @m.vote_average = @movie.vote_average
        @m.release_date = @movie.release_date
        @m.background = "http://image.tmdb.org/t/p/w780/" + @movie.backdrop_path
        @m.backdrop = "http://image.tmdb.org/t/p/w780/" + @movie.backdrop_path
        @m.save
        var += 1 
      end
      
    end    
  end

  def edit 

  end

  def new 
    @person = Person.new 
  end

  def create
    @person = Person.new actor_params
    if @person.save
      redirect_to dashboard_people_path, notice: "Se creo correctamente"
    else
      render 'new', notice: "Ops, ocurrio algun error"
    end
  end

  def update
    if @person.update person_params
      redirect_to dashboard_people_path, notice: "lo logramos, se actualizo"
    else
      redirecto_to 'edit', notice: "Algo salio mal"
    end
  end

  def destroy
    @person.destroy
    redirect_to people_path
  end

  private 

  def find_post
    @person = Person.friendly.find(params[:id])
  end

  def person_params
    params.require(:person).permit(:name, :biography, :birthday, :deathday, :homepage, :imdb_id, :tmdb_id, :image, :place_to_birth)
  end

end
