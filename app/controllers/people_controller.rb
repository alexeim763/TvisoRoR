class PeopleController < ApplicationController
  before_action :find_post, only: [:show, :edit, :destroy, :update]
  before_action :check_details, only: [:show]
  def index
    @people = Person.paginate(:page => params[:page], :per_page => 12)
  end

  def show

  end
  def film
    @person = Person.friendly.find(params[:person_id])
  end
  def new 
    @person = Person.new 
  end

  def create
    @person = Person.new person_params
    if @person.save
      redirect_to people_path, notice: "Se creo correctamente"
    else
      render 'new', notice: "Ops, ocurrio algun error"
    end
  end

  def update
    if @person.update people_path
      redirect_to @person, notice: "lo logramos, se actualizo"
    else
      redirecto_to 'edit', notice: "Algo salio mal"
    end
  end

  def destroy
    @person.destroy
    redirect_to people_path
  end

  private 

  def find_post
    @person = Person.friendly.find(params[:id])
  end

  def person_params
    params.require(:actor).permit(:name, :biography, :birthday, :deathday, :homepage, :imdb_id, :tmdb_id, :image, :place_to_birth)
  end
  def check_details
    if @person.imdb_id == nil
      new_person = Tmdb::Person.detail(@person.tmdb_id, language: I18n.locale.to_s)
      if new_person
        @person.biography = new_person.biography if new_person.biography 
        @person.birthday = new_person.birthday if new_person.birthday
        @person.deathday = new_person.deathday if new_person.deathday
        @person.homepage = new_person.homepage if new_person.homepage
        @person.imdb_id = new_person.imdb_id if new_person.imdb_id
        @person.place_to_birth = new_person.place_of_birth if new_person.place_of_birth
        @person.save
      end
    end
  end

end
