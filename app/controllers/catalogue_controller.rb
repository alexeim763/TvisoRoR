class CatalogueController < ApplicationController
  layout 'backblack'
  def index
    @titles = Title.paginate(:page => params[:page], :per_page => 12)
  end
end
