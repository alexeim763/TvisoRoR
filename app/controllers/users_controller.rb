class UsersController < ApplicationController
  layout 'backblack'
  before_action :set_user, only: [:show,:update]

  def show

  end
  def update
    if @user.update user_params
      redirect_to settings_path, notice: "lo logramos, se actualizo"
    else
      redirecto_to 'edit', notice: "Algo salio mal"
    end
  end 
  private
  def set_user
    @user = User.friendly.find(params[:id])
  end
  def user_params
    params.require(:user).permit(:avatar, :nickname, :first_name, :last_name, :email)
  end
end
