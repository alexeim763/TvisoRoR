class People::CommentsController < CommentsController
  before_action :set_commentable

  private 
  def set_commentable
    @commentable = Person.friendly.find(params[:person_id])
  end
end