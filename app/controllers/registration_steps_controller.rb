class RegistrationStepsController < ApplicationController
  include Wicked::Wizard
  steps :basic_info, :setting_account, :defined_tastes  

  def show
    @user = current_user
    
    render_wizard
  end
end
