class MoviesController < ApplicationController
  before_action :find_movie, only: [:show]
  before_action :check_trailer, only: [:show]
  before_action :check_details, only: [:show]
  before_action :check_cast, only: [:show]
  before_action :check_crew, only: [:show]


  def show
    @director = @movie.crews.where(job: 'Director')[0].person.name
  end

  private
  def find_movie
    @movie = Title.friendly.find(params[:id])
  end

  def check_trailer
    unless @movie.trailer
       @video = Tmdb::Movie.videos(@movie.tmbd_id)
       if @video
         sufix_video = "https://www.youtube.com/embed/"
         @movie.trailer = sufix_video + @video[0].key
         @movie.save 
       end
    end
  end

  def check_details
    if @movie.imbd_id == ""  && @movie.tagline == ""
      details = Tmdb::Movie.detail(@movie.tmbd_id, language: I18n.locale.to_s)
      if details
        @movie.tagline = details.tagline if details.tagline
        @movie.imbd_id = details.imdb_id
        @movie.runtime = details.runtime if details.runtime
        @movie.status = details.status if details.status 
        @movie.budget = details.budget if details.status 
        @movie.revenue = details.revenue if details.revenue
        @movie.website = details.website if details.website
        @movie.country = details.spoken_languages[0].iso_639_1
        @movie.save
      end
    end
  end

  def check_crew
   if @movie.crews.count == 0
      crews = Tmdb::Movie.crew(@movie.tmbd_id)
      crews.each do |crew|
        new_person = Person.new
        new_person.name = crew.name
        if crew.profile_path == nil
          new_person.image = ""
        else
          new_person.image = "https://image.tmdb.org/t/p/w185" + crew.profile_path
        end
        new_person.tmdb_id = crew.id
        person = Person.find_or_create_person(new_person,I18n.locale)
        if person
          new_crew = Crew.new
          new_crew.department = crew.department if crew.department
          new_crew.job = crew.job if crew.job
          new_crew.person_id = person.id
          new_crew.title_id = @movie.id
          new_crew = Crew.create_relation_title_person(new_crew)
          
        end  

      end   
    end
  end

  def check_cast
    if @movie.casts.count == 0
      casts = Tmdb::Movie.cast(@movie.tmbd_id)
      casts.each do |cast|
        new_person = Person.new
        new_person.name = cast.name
        if cast.profile_path == nil
          new_person.image = ""
        else
          new_person.image = "https://image.tmdb.org/t/p/w185" + cast.profile_path
        end
         new_person.tmdb_id = cast.id
         person = Person.find_or_create_person(new_person,I18n.locale)
        if person
          new_cast = Cast.new
          new_cast.person_id = person.id
          new_cast.title_id = @movie.id
          new_cast.character = cast.character
          new_cast = Cast.create_relation_title_person(new_cast)
          
        end  
      end
    end
  end
end
