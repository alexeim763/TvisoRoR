json.array!(@titles) do |title|
  json.extract! title, :id, :title, :type_id, :overview, :poster, :tagline, :trailer, :imbd_id, :tmbd_id, :vote_average, :release_date, :background, :runtime, :status, :budget, :revenue, :country
  json.url title_url(title, format: :json)
end
