class Cast < ActiveRecord::Base
  belongs_to :title
  belongs_to :person
  private
  def self.create_relation_title_person(object)
    relation = Cast.create(title_id: object.title_id, person_id: object.person_id, character: object.character)
    return relation
  end
end
