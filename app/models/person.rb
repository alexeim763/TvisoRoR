class Person < ActiveRecord::Base
  translates :biography
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :comments, as: :commentable

  has_many :crews
  has_many :titles, through: :crews
  
  has_many :casts
  has_many :titles, through: :casts
  validates :tmdb_id, uniqueness: true
  private
  def self.find_or_create_person(object, language)
    person = Person.where(tmdb_id: object.tmdb_id).first
    unless person
      I18n.locale = language
      person = Person.create(
        name: object.name,
        biography: object.biography,
        birthday: object.birthday,
        deathday: object.deathday,
        homepage: object.homepage,
        imdb_id: object.imdb_id,
        tmdb_id: object.tmdb_id,
        image: object.image,
        place_to_birth: object.place_to_birth,
        )
    end
    return person
  end
end
