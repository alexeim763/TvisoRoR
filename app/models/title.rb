class Title < ActiveRecord::Base
  belongs_to :type
  translates :title, :overview , :tagline
  extend FriendlyId
    friendly_id :title, use: :slugged
  has_many :crews
  has_many :people, through: :crews
  
  has_many :casts
  has_many :people, through: :casts
  

  validates :tmbd_id, uniqueness: true

  private
  def self.find_or_update_by_scrapper(object,id,language)
    title = Title.where(tmbd_id: id).first
    if title
      I18n.locale = language
      title = title.update(
        title: object.title,
        overview: object.overview,
        tagline: object.tagline
        )
    else
      I18n.locale = language
      title = Title.create(
        title: object.title,
        overview: object.overview,
        poster: object.poster,
        tagline: object.tagline,
        trailer: object.trailer,
        imbd_id: object.imbd_id,
        tmbd_id: object.tmbd_id,
        vote_average: object.vote_average,
        release_date: object.release_date,
        background: object.background,
        runtime: object.runtime,
        status: object.status,
        budget: object.budget,
        revenue: object.revenue,
        country: object.country,
        type_id: object.type_id,
        backdrop: object.backdrop,
        slug: object.slug,
        website: object.website
        )
    end
    title
  end
 
end
