class User < ActiveRecord::Base 
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_reader :avatar_remote_url
  has_many :comments
  extend FriendlyId
  friendly_id :nickname, use: :slugged
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:facebook, :twitter, :google_oauth2]
  def self.find_or_create_by_omniauth(auth)
    user = User.where(provider: auth[:provider], uid: auth[:uid]).first

    unless user
      user = User.create(
        first_name: auth[:first_name],
        last_name: auth[:last_name],
        nickname: auth[:username],
        email: auth[:email],
        uid: auth[:uid],
        provider: auth[:provider],
        password: Devise.friendly_token[0,20]
      )
    end
    user
  end

  def avatar_remote_url(url_value)
    self.avatar = URI.parse(url_value)
    @avatar_remote_url = url_value
  end
end
