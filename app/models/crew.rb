class Crew < ActiveRecord::Base
  belongs_to :title
  belongs_to :person

  private
  def self.create_relation_title_person(object)
    relation = Crew.create(title_id: object.title_id, person_id: object.person_id, department: object.department, job: object.job)
    return relation

  end
end
