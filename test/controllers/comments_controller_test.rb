require "test_helper"

describe CommentsController do
  let(:comment) { comments :one }

  it "gets index" do
    get :index
    value(response).must_be :success?
    value(assigns(:comments)).wont_be :nil?
  end

  it "gets new" do
    get :new
    value(response).must_be :success?
  end

  it "creates comment" do
    expect {
      post :create, comment: { commentable_id: comment.commentable_id, commentable_type: comment.commentable_type, content: comment.content }
    }.must_change "Comment.count"

    must_redirect_to comment_path(assigns(:comment))
  end

  it "shows comment" do
    get :show, id: comment
    value(response).must_be :success?
  end

  it "gets edit" do
    get :edit, id: comment
    value(response).must_be :success?
  end

  it "updates comment" do
    put :update, id: comment, comment: { commentable_id: comment.commentable_id, commentable_type: comment.commentable_type, content: comment.content }
    must_redirect_to comment_path(assigns(:comment))
  end

  it "destroys comment" do
    expect {
      delete :destroy, id: comment
    }.must_change "Comment.count", -1

    must_redirect_to comments_path
  end
end
