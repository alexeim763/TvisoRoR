require "test_helper"

describe TypesController do
  let(:type) { types :one }

  it "gets index" do
    get :index
    value(response).must_be :success?
    value(assigns(:types)).wont_be :nil?
  end

  it "gets new" do
    get :new
    value(response).must_be :success?
  end

  it "creates type" do
    expect {
      post :create, type: { name: type.name }
    }.must_change "Type.count"

    must_redirect_to type_path(assigns(:type))
  end

  it "shows type" do
    get :show, id: type
    value(response).must_be :success?
  end

  it "gets edit" do
    get :edit, id: type
    value(response).must_be :success?
  end

  it "updates type" do
    put :update, id: type, type: { name: type.name }
    must_redirect_to type_path(assigns(:type))
  end

  it "destroys type" do
    expect {
      delete :destroy, id: type
    }.must_change "Type.count", -1

    must_redirect_to types_path
  end
end
