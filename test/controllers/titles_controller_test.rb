require "test_helper"

describe TitlesController do
  let(:title) { titles :one }

  it "gets index" do
    get :index
    value(response).must_be :success?
    value(assigns(:titles)).wont_be :nil?
  end

  it "gets new" do
    get :new
    value(response).must_be :success?
  end

  it "creates title" do
    expect {
      post :create, title: { background: title.background, budget: title.budget, country: title.country, imbd_id: title.imbd_id, overview: title.overview, poster: title.poster, release_date: title.release_date, revenue: title.revenue, runtime: title.runtime, status: title.status, tagline: title.tagline, title: title.title, tmbd_id: title.tmbd_id, trailer: title.trailer, type_id: title.type_id, vote_average: title.vote_average }
    }.must_change "Title.count"

    must_redirect_to title_path(assigns(:title))
  end

  it "shows title" do
    get :show, id: title
    value(response).must_be :success?
  end

  it "gets edit" do
    get :edit, id: title
    value(response).must_be :success?
  end

  it "updates title" do
    put :update, id: title, title: { background: title.background, budget: title.budget, country: title.country, imbd_id: title.imbd_id, overview: title.overview, poster: title.poster, release_date: title.release_date, revenue: title.revenue, runtime: title.runtime, status: title.status, tagline: title.tagline, title: title.title, tmbd_id: title.tmbd_id, trailer: title.trailer, type_id: title.type_id, vote_average: title.vote_average }
    must_redirect_to title_path(assigns(:title))
  end

  it "destroys title" do
    expect {
      delete :destroy, id: title
    }.must_change "Title.count", -1

    must_redirect_to titles_path
  end
end
