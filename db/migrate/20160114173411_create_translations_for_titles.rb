class CreateTranslationsForTitles < ActiveRecord::Migration
  def self.up
    Title.create_translation_table!({
        title: :string,
        overview: :text,
        tagline: :string
      }, {
      migrate_data: true
    })
  end
  def self.down
    Title.drop_translation_table! migrate_data: true
  end
end
