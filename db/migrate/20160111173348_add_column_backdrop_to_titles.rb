class AddColumnBackdropToTitles < ActiveRecord::Migration
  def change
    add_column :titles, :backdrop, :string
  end
end
