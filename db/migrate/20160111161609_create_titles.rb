class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :title
      t.text :overview
      t.string :poster
      t.string :tagline
      t.string :trailer
      t.string :imbd_id
      t.string :tmbd_id
      t.decimal :vote_average
      t.string :release_date
      t.string :background
      t.string :runtime
      t.string :status
      t.string :budget
      t.string :revenue
      t.string :country

      t.timestamps null: false
    end
  end
end
