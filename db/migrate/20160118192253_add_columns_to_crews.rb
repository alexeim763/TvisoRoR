class AddColumnsToCrews < ActiveRecord::Migration
  def change
    add_column :crews, :department, :string
    add_column :crews, :job, :string
  end
end
