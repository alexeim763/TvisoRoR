class ChangeColumnVoteAverageToTitles < ActiveRecord::Migration
  def change
    change_column :titles, :vote_average, :float
  end
end
