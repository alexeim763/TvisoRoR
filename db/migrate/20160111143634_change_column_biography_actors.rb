class ChangeColumnBiographyActors < ActiveRecord::Migration
  def change
    change_column :actors, :biography, :text
  end
end
