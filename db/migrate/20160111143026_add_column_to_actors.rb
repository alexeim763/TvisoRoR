class AddColumnToActors < ActiveRecord::Migration
  def change
    add_column :actors, :imdb_id, :string
    add_column :actors, :tmdb_id, :string
    add_column :actors, :image, :string
  end
end
