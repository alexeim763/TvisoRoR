class CreateCrews < ActiveRecord::Migration
  def change
    create_table :crews do |t|
      t.references :title, index: true, foreign_key: true
      t.references :person, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
