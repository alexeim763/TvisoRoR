class AddSlugToTitles < ActiveRecord::Migration
  def change
    add_column :titles, :slug, :string
  end
end
