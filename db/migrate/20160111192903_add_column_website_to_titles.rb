class AddColumnWebsiteToTitles < ActiveRecord::Migration
  def change
    add_column :titles, :website, :string
  end
end
