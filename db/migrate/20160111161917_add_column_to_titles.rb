class AddColumnToTitles < ActiveRecord::Migration
  def change
    add_reference :titles, :type, index: true, foreign_key: true
  end
end
