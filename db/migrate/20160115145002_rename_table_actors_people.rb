class RenameTableActorsPeople < ActiveRecord::Migration
  def change
    rename_table :actors, :people
  end
end
