class CreateActors < ActiveRecord::Migration
  def change
    create_table :actors do |t|
      t.string :name
      t.string :biography
      t.string :birthday
      t.string :deathday
      t.string :homepage

      t.timestamps null: false
    end
  end
end
