# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160118192347) do

  create_table "casts", force: :cascade do |t|
    t.integer  "title_id",   limit: 4
    t.integer  "person_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "character",  limit: 255
  end

  add_index "casts", ["person_id"], name: "index_casts_on_person_id", using: :btree
  add_index "casts", ["title_id"], name: "index_casts_on_title_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.text     "content",          limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "user_id",          limit: 4
  end

  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "crews", force: :cascade do |t|
    t.integer  "title_id",   limit: 4
    t.integer  "person_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "department", limit: 255
    t.string   "job",        limit: 255
  end

  add_index "crews", ["person_id"], name: "index_crews_on_person_id", using: :btree
  add_index "crews", ["title_id"], name: "index_crews_on_title_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "people", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.text     "biography",      limit: 65535
    t.string   "birthday",       limit: 255
    t.string   "deathday",       limit: 255
    t.string   "homepage",       limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "imdb_id",        limit: 255
    t.string   "tmdb_id",        limit: 255
    t.string   "image",          limit: 255
    t.string   "place_to_birth", limit: 255
    t.string   "slug",           limit: 255
  end

  create_table "person_translations", force: :cascade do |t|
    t.integer  "person_id",  limit: 4,     null: false
    t.string   "locale",     limit: 255,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "biography",  limit: 65535
  end

  add_index "person_translations", ["locale"], name: "index_person_translations_on_locale", using: :btree
  add_index "person_translations", ["person_id"], name: "index_person_translations_on_person_id", using: :btree

  create_table "title_translations", force: :cascade do |t|
    t.integer  "title_id",   limit: 4,     null: false
    t.string   "locale",     limit: 255,   null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "title",      limit: 255
    t.text     "overview",   limit: 65535
    t.string   "tagline",    limit: 255
  end

  add_index "title_translations", ["locale"], name: "index_title_translations_on_locale", using: :btree
  add_index "title_translations", ["title_id"], name: "index_title_translations_on_title_id", using: :btree

  create_table "titles", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.text     "overview",     limit: 65535
    t.string   "poster",       limit: 255
    t.string   "tagline",      limit: 255
    t.string   "trailer",      limit: 255
    t.string   "imbd_id",      limit: 255
    t.string   "tmbd_id",      limit: 255
    t.float    "vote_average", limit: 24
    t.string   "release_date", limit: 255
    t.string   "background",   limit: 255
    t.string   "runtime",      limit: 255
    t.string   "status",       limit: 255
    t.string   "budget",       limit: 255
    t.string   "revenue",      limit: 255
    t.string   "country",      limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "type_id",      limit: 4
    t.string   "backdrop",     limit: 255
    t.string   "slug",         limit: 255
    t.string   "website",      limit: 255
  end

  add_index "titles", ["type_id"], name: "index_titles_on_type_id", using: :btree

  create_table "types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "nickname",               limit: 255
    t.string   "country",                limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.string   "slug",                   limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  add_foreign_key "casts", "people"
  add_foreign_key "casts", "titles"
  add_foreign_key "comments", "users"
  add_foreign_key "crews", "people"
  add_foreign_key "crews", "titles"
  add_foreign_key "titles", "types"
end
